import {
  EDIT_SINH_VIEN,
  LAY_CHI_TIET_SINH_VIEN,
  THEM_SINH_VIEN,
  UPDATE_SINH_VIEN,
  XOA_SINH_VIEN,
} from "../constant/sinhVienConstant,";

const initialState = {
  mangSinhVien: [
    {
      maSV: "1",
      hoTen: "nguyeenx van a",
      email: "hai@2003.com",
      soDienThoai: "13213",
    },
    {
      maSV: "2",
      hoTen: "nguyeenx van b",
      email: "haib@2003.com",
      soDienThoai: "13213",
    },
  ],
  sinhVienEdited: {
    maSV: "-1",
    hoTen: "",
    email: "",
    soDienThoai: "",
  },
  disabled: false,
};

export const managerReducer = (state = initialState, action) => {
  switch (action.type) {
    case THEM_SINH_VIEN: {
      // Kiểm tra rỗng
      if (action.newSinhVien.maSV.trim() === "") {
        alert("maSV is required");
        return { ...state };
      }
      // Kiểm tra tồn tại
      let mangSvUpdate = [...state.mangSinhVien];
      let index = mangSvUpdate.findIndex(
        (sinhVien) => sinhVien.maSV === action.newSinhVien.maSV
      );
      if (index !== -1) {
        alert("maSV already exist !");
      } else {
        mangSvUpdate.push(action.newSinhVien);
      }
      // Xử lý xong thì gán mangSinhVien mới vào
      // mangSinhVien còn lại
      state.mangSinhVien = mangSvUpdate;
      return { ...state };
    }
    case XOA_SINH_VIEN: {
      let index = state.mangSinhVien.findIndex(
        (sinhVien) => sinhVien.maSV === action.sinhVienId
      );

      let mangSvUpdate = [...state.mangSinhVien];
      mangSvUpdate.splice(index, 1);
      state.mangSinhVien = mangSvUpdate;
      return { ...state };
    }

    case EDIT_SINH_VIEN: {
      return { ...state, sinhVienEdited: action.sinhVien, disabled: true };
    }
    case UPDATE_SINH_VIEN: {
      // update lại sinhVienEdited
      state.sinhVienEdited = {
        ...action.newSinhVien,
      };
      // tìm sinhVienEdited trong mangSinhVien
      let mangSinhVienUpdate = [...state.mangSinhVien];
      let index = mangSinhVienUpdate.findIndex(
        (sinhVien) => sinhVien.maSV === state.sinhVienEdited.maSV
      );

      if (index !== -1) {
        mangSinhVienUpdate[index] = state.sinhVienEdited;
      }
      state.mangSinhVien = mangSinhVienUpdate;
      return { ...state, disabled: false };
    }
    default:
      return { ...state };
  }
};
